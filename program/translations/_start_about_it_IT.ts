<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="72"/>
        <source>MainWindow</source>
        <translation>IT: HUVUDFÖNSTER</translation>
    </message>
    <message>
        <source>Start About</source>
        <translation type="vanished">IT: STARTA OM</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="74"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="48"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="75"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="73"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation type="vanished">Il programma è destinato alla creazione di elenchi di file per il programma di installazione di NSIS.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="17"/>
        <source>This program&apos;s job is to test the About library.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="18"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translatorcomment>Stort tack till bovirus för den italienska översättningen och för många bra idéer som gjort programmet bättre.</translatorcomment>
        <translation>Molte grazie a bovirus per la traduzioen italiana. E per molte buone idee per rendere il programma migliore.</translation>
    </message>
</context>
</TS>
