<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../mainwindow.ui" line="20"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="72"/>
        <source>MainWindow</source>
        <translation>HUVUDFÖNSTER</translation>
    </message>
    <message>
        <source>Start About</source>
        <translation type="vanished">STARTA OM</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="33"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="74"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="48"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="75"/>
        <source>File</source>
        <translation>Arkiv</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="57"/>
        <location filename="../../build-code-Qt_6_4_1_MinGW_64_bit-Release/abouttest_autogen/include/ui_mainwindow.h" line="73"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <source>The program is intended for creating file lists for NSIS installer program.</source>
        <translation type="vanished">Programmet är avsett för att skapa fillistor för NSIS installationsprogram.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="17"/>
        <source>This program&apos;s job is to test the About library.</source>
        <translation>Det här programmets uppgift är att testa &quot;Om&quot; biblioteket.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="18"/>
        <source>Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation>Stort tack till bovirus för den italienska översättningen och för många bra idéer som gjort programmet bättre.</translation>
    </message>
</context>
</TS>
