

if [[ -x "/opt/Qt/6.4.1/gcc_64/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/6.4.1/gcc_64/bin/"
    
elif [[ -x "/opt/Qt/Qt5.15.7/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/Qt5.15.7/bin/"

else

	echo "Cannot find executable program files."
	exit 1
	
fi




${SOKVAG}/lconvert -i _start_about_sv_SE.ts _about_sv_SE.ts  -o about_sv_SE.ts
${SOKVAG}/lconvert -i _start_about_it_IT.ts _about_it_IT.ts  -o about_it_IT.ts

${SOKVAG}/lrelease about_sv_SE.ts
cp -vf about_sv_SE.qm ../code/i18n/

${SOKVAG}/lrelease about_it_IT.ts
cp -vf about_it_IT.qm ../code/i18n/



