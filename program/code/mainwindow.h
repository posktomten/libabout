#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QScreen>
#include <QDebug>
#include "about.h"
#define DISPLAY_NAME "libabout test"
#define EXECUTABLE_NAME "abouttest"
#define VERSION "1.0.1"
#define DISPLAY_VERSION VERSION
#define BUILD_DATE_TIME __DATE__ " " __TIME__
#define SOURCECODE "https://gitlab.com/posktomten/libabout"
#define WEBSITE "https://gitlab.com/posktomten/libabout/-/wikis/home"
#define CHANGELOG "https://gitlab.com/posktomten/libabout/-/blob/master/CHANGELOG"
#define LICENSE "GNU General Public License v3.0"
#define LICENSE_LINK "https://gitlab.com/posktomten/libabout/-/blob/master/LICENSE"
#define COPYRIGHT "Ingemar Ceicer"
#define EMAIL "programming@ceicer.com"
#define COPYRIGHT_YEAR "2022"

#if defined(Q_OS_LINUX)
#if (__GLIBC_MINOR__ == 27)
#if (Q_PROCESSOR_WORDSIZE == 4) // 32 bit
#define COMPILEDON "Ubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#elif Q_PROCESSOR_WORDSIZE == 8
#define COMPILEDON "Ubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#endif // Q_PROCESSOR_WORDSIZE
#elif (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Ubuntu 20.04.5 LTS 64-bit, GLIBC 2.31"
#elif (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.1 LTS 64-bit, GLIBC 2.35"
#endif // __GLIBC_MINOR__
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#define FONTSIZE 14
#elif (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
#define FONTSIZE 13
#endif // QT_VERSION

#elif defined(Q_OS_WINDOWS)
#if defined(Q_PROCESSOR_X86_64)
#define COMPILEDON "Windows 11 Pro Version 23H2 x86_64<br>OS Build: 22631.3447"
#elif defined(Q_PROCESSOR_X86_32)
#define COMPILEDON "Windows 11 Pro Version 23H2 x86_64<br>OS Build: 22631.3447"
#endif // Q_PROCESSOR
#if (QT_VERSION >= QT_VERSION_CHECK(6, 0, 0))
#define FONTSIZE 11
#elif QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#define FONTSIZE 11
#endif // QT_VERSION
#endif //Q_OS





QT_BEGIN_NAMESPACE
namespace Ui
{
class MainWindow;
}

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;


};
#endif // MAINWINDOW_H
