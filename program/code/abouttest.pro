QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

equals(QT_MAJOR_VERSION, 5) {
CONFIG += c++14
}

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

TRANSLATIONS += \
    i18n/_start_about_sv_SE.ts \
    i18n/_start_about_it_IT.ts \
    i18n/_start_about_template_xx_XX.ts

TARGET = abouttest
win32:RC_FILE = myapp.rc
win32:RC_ICONS += images/icon.ico

equals(QT_MAJOR_VERSION, 5) {
INCLUDEPATH += "../include"
DESTDIR="../build-executable5"
CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug
}
equals(QT_MAJOR_VERSION, 6) {
#message($$QMAKE_CXX)
DESTDIR="../build-executable6"
INCLUDEPATH += "../include"
equals(QMAKE_CXX, cl) {
message(MSVC $$QMAKE_CXX)
CONFIG (release, debug|release): LIBS += -L../lib6_msvc/ -labout # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6_msvc/ -laboutd # Debug
}
equals(QMAKE_CXX, g++) {
message(GCC for Windows $$QMAKE_CXX)
CONFIG (release, debug|release): LIBS += -L../lib6/ -labout # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -laboutd # Debug
}

}

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resurser.qrc
