#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include <QFontDatabase>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont *font = new QFont(family);
    font->setFamily(family);
    font->setPointSize(FONTSIZE);
    a.setFont(*font);
    /*
    QTranslator *qtTranslator = new QTranslator;

    if(qtTranslator->load(":/i18n/about_sv_SE.qm")) {
        qDebug() << a.installTranslator(qtTranslator);
    } else {
        qDebug() << "Not loaded!";
    }
    */
    MainWindow w;
    w.show();
    return a.exec();
}
