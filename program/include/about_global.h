// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libabout
//          Copyright (C) 2022 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libabout
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#if !defined(ABOUT_GLOBALH)
#define ABOUT_GLOBALH
#include <QtCore/QtGlobal>

#if defined(ABOUT_LIBRARY)
#define ABOUT_EXPORT Q_DECL_EXPORT
#else
#define ABOUT_EXPORT Q_DECL_IMPORT
#endif // ABOUT_LIBRARY
#endif // ABOUT_GLOBALH

