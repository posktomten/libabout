// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libabout
//          Copyright (C) 2022 - 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/libabout
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#if !defined(ABOUT_H)
#define ABOUT_H



#include <QWidget>
#include <QMessageBox>
#include <QTranslator>
#include <QApplication>
#include <QDebug>
#include <QIcon>



QT_BEGIN_NAMESPACE
namespace Ui
{
class About;
}
QT_END_NAMESPACE


#if defined(Q_OS_LINUX)
#if defined(ABOUT_LIBRARY)
#include "about_global.h"
class ABOUT_EXPORT About : public QWidget
#else
class About : public QWidget
#endif
#else
class About : public QWidget
#endif // Q_OS_LINUX
{
    Q_OBJECT

public:
    About(const QString &display_name,
          const QString &display_version,
          const QString &copyright,
          const QString &email,
          const QString *copyright_year,
          const QString &builddatetime,
          const QString &license,
          const QString &license_link,
          const QString &changelog,
          const QString &sourcecode,
          const QString &website,
          const QString &compiledon,
          const QString *purpose,
          const QString *translator,
          const QPixmap *pixmap,
          bool ontop);

    ~About();
private:

    void info(QString &version, QString &revision, QString &qt, QString &bitar, QString &runningon, QString &location);

};
#endif // ABOUT_H
