#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          libabout
#//          Copyright (C) 2022 - 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/libabout
#//          programmering1@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License as published by
#//   the Free Software Foundation version 3.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
# equals(QT_MAJOR_VERSION, 5) {
# CONFIG += c++14
# }
#CONFIG += hide_symbols
CONFIG += staticlib


#QMAKE_CXXFLAGS_RELEASE += -DNDEBUG
message($$QMAKE_CXXFLAGS_RELEASE)
#compiler=$$VERSION(QMAKESPEC)
message($$CONFIG)

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
 #DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    about.cpp

HEADERS += \
    about.h \
    about_global.h

#Export LIBS
DEFINES += ABOUT_LIBRARY

TEMPLATE = lib

TRANSLATIONS += \
    i18n/_about_sv_SE.ts \
    i18n/_about_it_IT.ts \
    i18n/_about_template_xx_XX.ts

CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG


equals(BUILD,RELEASE) {

    TARGET=about

}
equals(BUILD,DEBUG) {

    TARGET=aboutd
}

equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
}

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
