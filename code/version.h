
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             2,19,6,0
#define VER_FILEVERSION_STR         "2.19.6.0\0"

#define VER_PRODUCTVERSION          2,19,6,0
#define VER_PRODUCTVERSION_STR      "2.19.6\0"

#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "streamCapture2"
#define VER_INTERNALNAME_STR        "streamCapture2"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2016 - 2024 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "streamcapture2.exe"
#define VER_PRODUCTNAME_STR         "streamCapture2"

#define VER_COMPANYDOMAIN_STR       "https://gitlab.com/posktomten/streamcapture2"

#endif // VERSION_H

