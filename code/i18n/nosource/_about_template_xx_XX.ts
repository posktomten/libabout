<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>About</name>
    <message>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright © </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiled on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is in the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Programming language: C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application framework: Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 32-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 64-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
