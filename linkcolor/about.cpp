// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libabout
//          Copyright (C) 2022 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/libabout
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "about.h"
#include <QDir>
About::About(const QString &display_name,
             const QString &display_version,
             const QString &copyright,
             const QString &email,
             const QString *copyright_year,
             const QString &builddatetime,
             const QString &license,
             const QString &license_link,
             const QString &changelog,
             const QString &sourcecode,
             const QString &website,
             const QString &compiledon,
             const QString *purpose,
             const QString *translator,
             const QPixmap *pixmap,
             const QString &linkcolor,
             bool ontop)
{
    if(pixmap != nullptr) {
        QApplication::setWindowIcon(QIcon(*pixmap));
    }

// #if defined(Q_OS_WINDOWS)
    // QFont font = QApplication::font();
    // font.setPointSize(10);
    // QApplication::setFont(font);
    // qDebug() << QApplication::font();
// #endif
    QString version, revision, qt, bitar, runningon, location;
    info(version, revision, qt, bitar, runningon, location);
    QMessageBox *msgBox = new QMessageBox;

    if(ontop) {
        msgBox->setWindowFlags(Qt::WindowStaysOnTopHint | Qt::WindowSystemMenuHint);
    }

    msgBox->setWindowTitle(tr("About ") + display_name + " " + display_version);
    msgBox->setText(QStringLiteral("<h1 style=\"color:green\">") + display_name + QStringLiteral(" ") + display_version + QStringLiteral("</h1><p>") + tr("Copyright © ") + copyright + QStringLiteral(" ") + *copyright_year +  QStringLiteral("<br>") + email + QStringLiteral("</p><p>") + tr("License: ") + QStringLiteral("<a style=\"text-decoration: none; color:") + linkcolor + QStringLiteral("; \" href =\"") + license_link + QStringLiteral("\"> ") + license + QStringLiteral(" </a > </p> <p>") + *purpose + QStringLiteral(" </p><p>") + *translator
                    +                                                                                                                                                                                                                                                                                                                                                                                                                     QStringLiteral("<p>") + display_name + QStringLiteral(" ") + tr("is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.") + "</p><p><nobr>" + (tr("See the GNU General Public License version 3.0 for more details.") + "</nobr>") +
                    QStringLiteral("<p><a style=\"text-decoration: none; color:") + linkcolor + QStringLiteral("; \" href =\"") + sourcecode + QStringLiteral("\">") + tr("Source code") + QStringLiteral("</a> | ") +
                    QStringLiteral("<a style=\"text-decoration: none; color:") + linkcolor + QStringLiteral("; \" href =\"") + website + QStringLiteral("\">") + tr("Website") + QStringLiteral("</a> | <a style=\"text-decoration: none; color:") + linkcolor + QStringLiteral("; \" href =\"") +  changelog + QStringLiteral("\">") + tr("Version history") + QStringLiteral("</a></p><p>") + revision + QStringLiteral("<br>") + version + QStringLiteral("<br>") +
                    // QStringLiteral("<a style=\"text-decoration: none; color:") + linkcolor + + qt + QStringLiteral("</a>")
                    qt
                    + QStringLiteral("<br>") + bitar + QStringLiteral("<br>") + tr("Compiled on: ") + compiledon  + QStringLiteral("<br>") + display_name + tr(" is in the folder: ") + QStringLiteral("<br>") + location + QStringLiteral("<br>")  + tr("Created: ")  + builddatetime +  QStringLiteral("<br>") +  tr("Runs on: ") + runningon + QStringLiteral("</p>"));
    msgBox->show();
}

void About::info(QString &version, QString &revision, QString &qt, QString &bitar, QString &runningon, QString &location)
{
#ifdef __GNUC__
    version = (QString(tr("Compiler:") + " %1 %2.%3.%4")
               .arg("GCC")
               .arg(__GNUC__)
               .arg(__GNUC_MINOR__)
               .arg(__GNUC_PATCHLEVEL__));
#endif
#ifdef __clang__
// #ifdef Q_OS_LINUX
    version = (QString(tr("Compiler: Clang version ") + " %1.%2.%3")
               .arg(__clang_major__)
               .arg(__clang_minor__)
               .arg(__clang_patchlevel__));
// #endif
// #elif defined Q_OS_WINDOS
#ifdef Q_OS_WINDOS
    version = (QString(tr("Compiler: LLVM/Clang/LLD based MinGW version ") + " %1.%2.%3")
               .arg(__clang_major__)
               .arg(__clang_minor__)
               .arg(__clang_patchlevel__));
#endif
#elif __MINGW32__
    version = (QString(tr("Compiler: MinGW (GCC for Windows) version ") + " %1.%2.%3")
               .arg(__GNUC__)
               .arg(__GNUC_MINOR__)
               .arg(__GNUC_PATCHLEVEL__));
#endif
#ifdef _MSC_VER

    if(_MSC_VER == 1940) {
        version = tr("Compiler:") + " Microsoft Visual Studio 2022 Version 17.10";
    } else if(_MSC_VER == 1941) {
        version = tr("Compiler:") + " Microsoft Visual Studio 2022 Version 17.11";
    } else if(_MSC_VER == 1942) {
        version = tr("Compiler:") + " Microsoft Visual Studio 2022 Version 17.12";
    }

#endif
#if __cplusplus==202002L
    revision = tr("Programming language: C++") + "<br>" + tr("C++ version: C++20");
#elif __cplusplus==201703L
    revision = tr("Programming language: C++") + "<br>" + tr("C++ version: C++17");
#elif __cplusplus==201402L
    revision = tr("Programming language: C++") + "<br>" + tr("C++ version: C++14");
#elif __cplusplus==201103L
    revision = tr("Programming language: C++") + "<br>" + tr("C++ version: C++11");
#else
    revision = tr("Programming language: C++") + "<br>" + tr("C++ version: Unknown");
#endif
    qt = tr("Application framework: Qt version ") + QT_VERSION_STR;
#ifdef Q_PROCESSOR_X86_32 // 32 bit
    bitar = tr("Processor architecture: 32-bit");
#endif
#ifdef Q_PROCESSOR_X86_64 // 64 bit
    bitar = tr("Processor architecture: 64-bit");
#endif
    runningon =  QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture();
    location = "\"" + QDir::toNativeSeparators(QApplication::applicationDirPath()) + "\"";
}

About::~About()
{
}
