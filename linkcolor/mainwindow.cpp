#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "about.h"
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    move(screen()->geometry().center() - frameGeometry().center());
    setFixedSize(296, 128);
    connect(ui->actionExit, &QAction::triggered, [this]() {
        close();
    });

    this->setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
    this->setWindowIcon(QIcon(":/images/icon.ico"));
    connect(ui->pbAbout, &QPushButton::clicked, []() {
        const QString *purpose = new QString(tr("This program's job is to test the About library."));
        const QString *translator = new QString(tr("Many thanks to bovirus for the Italian translation. And for many good ideas that have made the program better."));
//        const QString *translator = new QString("");
        const QPixmap *pixmap = new QPixmap(":/images/icon.png");
        const QString *copyright_year = new QString(COPYRIGHT_YEAR);
//      const QPixmap *pixmap = nullptr;
        const QString linkcolor("red");
        About *about = new About(DISPLAY_NAME,
                                 DISPLAY_VERSION,
                                 COPYRIGHT,
                                 EMAIL,
                                 copyright_year,
                                 BUILD_DATE_TIME,
                                 LICENSE,
                                 LICENSE_LINK,
                                 CHANGELOG,
                                 SOURCECODE,
                                 WEBSITE,
                                 COMPILEDON,
                                 purpose,
                                 translator,
                                 pixmap,
                                 linkcolor,
                                 false);
        delete about;
    });
}

MainWindow::~MainWindow()
{
    delete ui;
}

